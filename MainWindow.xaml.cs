﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Esri.ArcGISRuntime.Geometry;
using Esri.ArcGISRuntime.Mapping;
using Esri.ArcGISRuntime.Ogc;
using System.Collections.ObjectModel;
using System.Data.OleDb;
using System.Data;
using System.Windows.Threading;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace GIS3LR
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            mapView.Map = new Esri.ArcGISRuntime.Mapping.Map();
            mapView.Map.Basemap = Esri.ArcGISRuntime.Mapping.Basemap.CreateImagery();
            KmlLayer kml_layer = new KmlLayer(new Esri.ArcGISRuntime.Ogc.KmlDataset(new System.Uri(@"C:\Users\Oleg\Google Drive\10 триместр\ГИС\КМ3\data\vector.kml")));
            // Добавляем слой на карту
            mapView.Map.OperationalLayers.Add(kml_layer);
            
            
            // Create a map point the map should zoom to
            MapPoint mapPoint = new MapPoint(6380000, 9915000, SpatialReferences.WebMercator);

            // Set the initial viewpoint for map
            mapView.Map.InitialViewpoint = new Viewpoint(mapPoint, 300000);

            //// Event for layer view state changed
            //MyMapView.LayerViewStateChanged += OnLayerViewStateChanged;

            //// Provide used Map to the MapView
            //mapView.Map = myMap;

            InitializeList();
            listView.SelectedIndex = 0;
        }
        private void InitializeList()
        {
            //listView.Items.Clear();
            GridView gv = new GridView();
            gv.AllowsColumnReorder = true;
            GridViewColumn gvc = new GridViewColumn();
            gvc.Header = "Name";
            gv.Columns.Add(gvc);            
            this.listView.View = gv;

            Information();
            OleDbDataReader read = Select("SELECT * FROM Land"); // запрашиваем данные
            while (read.Read())
            { // обрабатываем данные
                listView.Items.Add(read.GetValue(6));
                //MessageBox.Show("ID: " + read.GetValue(0) + " Кодастровый номер: " + read.GetValue(1) + " Субъект федерации: " + read.GetValue(2)); // выводим данные
            }
        }
        public string connection = @"Provider=Microsoft.ACE.OLEDB.12.0; Data Source=C:\Users\Oleg\Google Drive\10 триместр\ГИС\КМ3\data\Data.mdb"; // путь к базе данных

        public OleDbDataReader Select(string selectSQL) // функция подключения к базе данных и обработка запросов
        {
            OleDbConnection connect = new OleDbConnection(connection); // подключаемся к базе данных
            connect.Open(); // открываем базу данных

            OleDbCommand cmd = new OleDbCommand(selectSQL, connect); // создаём запрос
            OleDbDataReader reader = cmd.ExecuteReader(); // получаем данные

            return reader; // возвращаем
        }

        public void Information()
        {
            System.Data.DataTable table = new OleDbEnumerator().GetElements();
            string inf = "";
            foreach (DataRow row in table.Rows)
                inf += row["SOURCES_NAME"] + "\n";

            MessageBox.Show(inf);
        }

        private void BtnNext_Click(object sender, RoutedEventArgs e)
        {           
            if (listView.SelectedIndex == listView.Items.Count-1)
                listView.SelectedIndex = 0;
            else listView.SelectedIndex += 1;
        }

        private void BtnPrev_Click(object sender, RoutedEventArgs e)
        {
            if (listView.SelectedIndex == 0)
                listView.SelectedIndex = listView.Items.Count - 1;
            else
                listView.SelectedIndex -= 1;
        }

        private void BtnView_Click(object sender, RoutedEventArgs e)
        {
            OleDbDataReader read = Select("SELECT * FROM Land WHERE [Описание местоположения] = '" + listView.SelectedItem.ToString() + "'"); // запрашиваем данные
            while (read.Read())
            { // обрабатываем данные
                //listView.Items.Add(read.GetValue(6));
                MessageBox.Show("ID: " + read.GetValue(0) + " Кодастровый номер: " + read.GetValue(1) + " Субъект федерации: " + read.GetValue(2)); // выводим данные
                MessageBox.Show(listView.SelectedItem.ToString());
            }
        }
    }
}
